/* jslint node: true */
"use strict";

var gulp        = require('gulp'),
    gutil       = require('gulp-util'),
    uglify      = require('gulp-uglify'),
    concat      = require('gulp-concat'),
    minifyCSS   = require('gulp-minify-css'),
    changed     = require('gulp-changed'),
    imagemin    = require('gulp-imagemin'),
    minifyHTML  = require('gulp-minify-html'),
    stripDebug  = require('gulp-strip-debug'), 
    clean       = require('gulp-clean'),
    sequence    = require('run-sequence'),
    htmlreplace = require('gulp-html-replace'),
    replace     = require('gulp-batch-replace');
    
    // Package information, including version
    var pkg = require('../../package.json');
    
    var info = {};
 
    info.src = {
        html         : '../../../app/views/index.php',
        js           : '../../app/js/**/*.js',
        css          : '../../app/css/**/*.css',
        img          : '../../app/img/**/*',
        fonts        : '../../app/fonts/**/*',
        bower        : '../../app/bower_components/**/*',
        clean        : '../../dist/**/*',
        html_popup   : '../../app/popup/**/*.html',
        html_partials: '../../app/partials/**/*.html',
        assets       : '../../app/assets/**/*',
        
        app          : '../../../app/**/*',
        bootstrap    : '../../../bootstrap/**/*',
        vendor       : '../../../vendor/**/*'
    };

    info.dest = {
        html         : '../../dist/public_html',
        js           : '../../dist/public_html/app/js',
        css          : '../../dist/public_html/app/css',
        img          : '../../dist/public_html/app/img',
        fonts        : '../../dist/public_html/app/fonts',
        bower        : '../../dist/public_html/app/bower_components',
        html_popup   : '../../dist/public_html/app/popup',
        html_partials: '../../dist/public_html/app/partials',  
        assets       : '../../dist/public_html/app/assets',
        
        app          : '../../dist/app',
        bootstrap    : '../../dist/bootstrap',
        vendor       : '../../dist/vendor'
    };
    
    // ignore folders
    info.ignore = {
        barryvdh     : '!../../../vendor/barryvdh/**',
        way          : '!../../../vendor/way/**'
    };
    
    var task = {
        clean         : 'clean',
        imagemin      : 'imagemin',
        minify_js     : 'minify_js',
        copy_fonts    : 'copy_fonts',
        copy_bower    : 'copy_bower',
        copy_assets   : 'copy_assets',
        minify_css    : 'minify_css',
        htmlpage      : 'htmlpage',
        html_partials : 'html_partials',
        html_popup    : 'html_popup',
        
        copy_app      : 'copy_app',
        copy_bootstrap: 'copy_bootstrap',
        copy_vendor   : 'copy_vendor',
        
        build         : 'build'
    };
    
    // Clean build folder
    gulp.task(task.clean, function () {
        return gulp.src(info.src.clean, {read: false})
            .pipe(clean());
    });
    
    // minify new images
    gulp.task(task.imagemin, function() {
        return gulp.src(info.src.img)
            .pipe(changed(info.dest.img))
            .pipe(imagemin())
            .pipe(gulp.dest(info.dest.img));
    });
    
    // Build JS files
    gulp.task(task.minify_js, function() {
        gulp.src(info.src.js)        
            .pipe(concat('main-' + pkg.version + '.js'))
            .pipe(stripDebug())
            .pipe(uglify())
            .pipe(gulp.dest(info.dest.js));
    });
    
    gulp.task(task.copy_fonts, function() {
        gulp.src([
            info.src.fonts + '.eot', 
            info.src.fonts + '.svg', 
            info.src.fonts + '.ttf', 
            info.src.fonts + '.woff'])
            .pipe(gulp.dest(info.dest.fonts));
    });

    gulp.task(task.copy_bower, function() {
        gulp.src([
            info.src.bower + '.min.js', 
            info.src.bower + '.min.css', 
            info.src.bower + '.png', 
            info.src.bower + '.eot', 
            info.src.bower + '.svg', 
            info.src.bower + '.ttf', 
            info.src.bower + '.woff'])
            .pipe(gulp.dest(info.dest.bower));
    });
    
    gulp.task(task.copy_assets, function() {
        gulp.src([info.src.assets])
            .pipe(gulp.dest(info.dest.assets));
    });
    
    gulp.task(task.minify_css, function() {
        gulp.src(info.src.css)
            .pipe(concat('main-' + pkg.version + '.css'))
            .pipe(minifyCSS({keepBreaks:true}))
            .pipe(gulp.dest(info.dest.css));
    });    
    
    // Build HTML files
    gulp.task(task.htmlpage, function(){
        
        var opts = {comments:true,spare:true};
        
        var replaceThis = [
            [ '.js',  '.min.js'  ],
            [ '.css', '.min.css' ]
        ];

        gulp.src(info.src.html)
            .pipe(changed(info.dest.html))            
            .pipe(replace(replaceThis))
            .pipe(htmlreplace({
                'css': 'app/css/main-' + pkg.version + '.css',
                'js' : 'app/js/main-'  + pkg.version + '.js'
            }))
            .pipe(minifyHTML(opts))
            .pipe(gulp.dest(info.dest.html));
    });
    
    // Build HTML popup files
    gulp.task(task.html_popup, function() {
        
        var opts = {comments:true,spare:true};
        
        gulp.src(info.src.html_popup)
            .pipe(changed(info.dest.html_popup))            
            .pipe(minifyHTML(opts))
            .pipe(gulp.dest(info.dest.html_popup));
    });
    
    // Build HTML partials files
    gulp.task(task.html_partials, function() {
        
        var opts = {comments:true,spare:true};
        
        gulp.src(info.src.html_partials)
            .pipe(changed(info.dest.html_partials))            
            .pipe(minifyHTML(opts))
            .pipe(gulp.dest(info.dest.html_partials));
    });
    
    gulp.task(task.copy_app, function() {
        gulp.src([info.src.app])
            .pipe(gulp.dest(info.dest.app));
    });
    
    gulp.task(task.copy_bootstrap, function() {
        gulp.src([info.src.bootstrap])
            .pipe(gulp.dest(info.dest.bootstrap));
    });
    
    gulp.task(task.copy_vendor, function() {
        gulp.src([info.src.vendor, 
            info.ignore.barryvdh,  
            info.ignore.way])
            .pipe(gulp.dest(info.dest.vendor));
    });
    
    // Full build - clean build folder, build js, build html
    gulp.task(task.build, function(callback) {
        sequence(
            task.clean, 
            task.imagemin, 
            task.minify_js, 
            task.minify_css, 
            task.copy_fonts,
            task.copy_bower,
            task.copy_assets,
            task.htmlpage,       
            task.html_partials,
            task.html_popup,
            
            task.copy_app,
            task.copy_bootstrap,
            task.copy_vendor,
            callback
        );
    });

    gulp.task('default', [task.build]);


