# Guia para proyectos 

* * *

## Tabla de contenidos

* [Proposito](https://bitbucket.org/danicode/project_structure_laravel_angularjs/overview#markdown-header-proposito)

* [Requirements](https://bitbucket.org/danicode/project_structure_laravel_angularjs/overview#markdown-header-requirements)

* [Caracteristicas del proyecto](https://bitbucket.org/danicode/project_structure_laravel_angularjs/overview#markdown-header-caracteristicas-del-proyecto)
    * [Composer](https://bitbucket.org/danicode/project_structure_laravel_angularjs/overview#markdown-header-composer)
    * [Consideracion](https://bitbucket.org/danicode/project_structure_laravel_angularjs/overview#markdown-header-consideracion)

* [Deployment](https://bitbucket.org/danicode/project_structure_laravel_angularjs/overview#markdown-header-deployment)
    * [Dependecias](https://bitbucket.org/danicode/project_structure_laravel_angularjs/overview#markdown-header-dependecias)
    * [Supuestos para el deploy](https://bitbucket.org/danicode/project_structure_laravel_angularjs/overview#markdown-header-supuestos-para-el-deploy)

* * *

## Proposito

El **objetivo** de este repositorio es tener una **estructura base** para **iniciar proyectos** con las tecnologias nombradas en los requerimientos.

* * *

## Requirements

- Backend: [Laravel 4](http://laravel.com/).

- Frontend: [AngulaJS](https://angularjs.org/).

- Webserver: [Apache2](httpd.apache.org/).

- Data base: [MySQL](http://www.mysql.com/).

- Other technologies: [BowerJS](http://bower.io//), [GulpJS](http://gulpjs.com/), [NodeJS](http://nodejs.org/), [Composer](https://getcomposer.org/).

- Version control: [Git](http://git-scm.com/).

- Repository: [Bitbucket](https://bitbucket.org/).
    



* * *

## Caracteristicas del proyecto

### Composer

Se debe descargar con **Composer** los paquetes:

    "require": {
        "laravel/framework": "4.1.*",
        "barryvdh/laravel-ide-helper": "1.*",
        "barryvdh/laravel-debugbar": "1.*",
        "barryvdh/laravel-vendor-cleanup": "1.*",
        "way/generators": "2.*"                
    },

*La carpeta **public** se cambio por **public_html**, para que pueda correr en **hosting 
compartidos**.

Para ello se debe dirigir a la carpeta **bootstrat** y realizar el siguiente cambio

    'public' => __DIR__.'/../public'

Por esto:

    'public' => __DIR__.'/../public_html'

### Consideracion 

Esta preparada para soportar el **entorno local**

Para ello se debe agregar en la carpeta **app/config** la carpeta **local** y luego

Se debe agregar los archivos necesarios y que desea tener encuenta, por ej:

**app.php**

    <?php

    return array(

        /*
        |--------------------------------------------------------------------------
        | Application Debug Mode
        |--------------------------------------------------------------------------
        |
        | When your application is in debug mode, detailed error messages with
        | stack traces will be shown on every error that occurs within your
        | application. If disabled, a simple generic error page is shown.
        |
        */

       'debug'    => true,
       'Debugbar' => 'Barryvdh\Debugbar\Facade',

    );

Esto es para que funcione el debug bar de la libreria de **Barryvdh\Debugbar**.

**database.php**

    <?php

    return array(

        'connections' => array(

        'mysql' => array(
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => '',
            'username'  => 'root',
            'password'  => '',
            'charset'   => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix'    => '',
        ),
    ));

Este archivo es con la configuracion para utilizar la base de datos **MySQL local**.

*Requisito para que esto funcione se debe ir a: **bootstrap/start.php**

    'local' => array('your-machine-name'),

Y **agregar el nombre de su maquina**. Para poder obtener el nombre de su maquina, 

en caso de que su SO sea Ubuntu, se debe abrir la consola y ejecutar el comando

    hostname

esto nos devolvera el nombre de la maquina y luego colocamos ese 

nombre en start.php. Por ej.:

    'local' => array('my_pc'),

* * *

## DEPLOYMENT

### Dependecias: 

    - NodeJS
    - GulpJS
    - BowerJS
    - ademas de las dependencias de bower.json y package.json.

En la carpeta: **public_html/build**

Tenemos 2 carpetas una 

**"deploy_build_init"**: que es para la primera vez que se va 
a empujar a produccion por ej. Debido a que sube la carpeta vendor y otras que
para las proximas deploys no se subiran, salvo excepciones.

Para poder hacer eso debemos abrir la consola y posicionarnos en está carpeta
y ejecutar el siguiente comando

    gulp

**"deploy_build"**: es para hacer el deploy, luego de haber hecho previamente, el 
deploy pero con la carpeta **deploy_build_init**.

Para poder hacer eso debemos abrir la consola y posicionarnos en está carpeta
y ejecutar el siguiente comando

    gulp

Con esto se generara una carpeta en **public_html/dist**

Esta carpeta **dist** hay que subirla al servidor.


## Supuestos para el deploy

**Generales:**

    Al crear el **deploy** se genera un carpeta **dist** dentro esta tanto **Laravel** como **AngularJS**.

**Fronted**

    - Todas las **librerias externas** solamente se miniarizan, pero no se concatenan

    - Todas los **javascripts** propios se miniarizan y se concatenan en un solo js, llamado: **main.js**

    - Todas los **css** propios se miniarizan y se concatenan en un solo css, llamado: **main.css**

**Backend**

    - La primera vez se debe utilizar el archivo **gulp.js** que se encuentra en el directorio **public_html/build/build_deploy_init**.
    Esto se debe a que la primera vez se debe enviar algunos directorios por ej.: **vendor**.

    - Las siguientes veces que se haga el deploy se debe utilizar el archivo **gulp.js** que se encuentra en el directorio 
    **public_html/build/build_deploy**.
